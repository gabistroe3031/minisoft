﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MiniSvUsersEntities.Data
{
    public partial class File
    {
        public int FileId { get; set; }
        public int UserId { get; set; }
        public string FileName { get; set; }

        public virtual User User { get; set; }
    }
}
