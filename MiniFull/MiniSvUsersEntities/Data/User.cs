﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MiniSvUsersEntities.Data
{
    public partial class User
    {
        public User()
        {
            Files = new HashSet<File>();
        }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public virtual ICollection<File> Files { get; set; }
    }
}
