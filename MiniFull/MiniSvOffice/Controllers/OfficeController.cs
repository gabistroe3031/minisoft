﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MiniSvOffice.Model;
using MiniSvOfficeEntities;
using RabbitMQManager;
using RabbitMQManager.DataStructure;

namespace MiniSvOffice.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OfficeController : ControllerBase
    {       
        private readonly ILogger<OfficeController> _logger;
        private readonly IRabbitMq _rabbitMq;
        private readonly SVOfficeContext _ctx;

        public OfficeController(ILogger<OfficeController> logger , IRabbitMq rabbitMq, SVOfficeContext ctx)
        {
            _logger = logger;
            _rabbitMq = rabbitMq;
            _ctx = ctx;
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        [Route("getall")]
        public IEnumerable<ModelOffice> GetAll()
        {
            /*var query = from o in _ctx.Offices
                      join r in _ctx.Rooms on o.OfficeId equals r.OfficeId
                      select new { o, r };
            var dbRes = query.ToList();*/

            _ctx.Offices.Include(x=>x.Rooms).ThenInclude(y=>y.Employees).ToList().ForEach(o => {
                o.Rooms.ToList().ForEach(r =>
                {
                    r.Employees.ToList().ForEach(e =>
                    {
                        ReqUserData aReq = new ReqUserData() { UserID = e.UserId };
                        //_rabbitMq.SendData(aReq);

                    });
                });
            });


            return new List<ModelOffice>();
        }
    }
}
