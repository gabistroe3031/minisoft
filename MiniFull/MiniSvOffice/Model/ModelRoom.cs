﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniSvOffice.Model
{
    public class ModelRoom
    {
        public int RoomId { get; set; }
        public int OfficeId { get; set; }
        public string RoomDesc { get; set; }

        public virtual ICollection<ModelEmployee> Employees { get; set; }
    }
}
