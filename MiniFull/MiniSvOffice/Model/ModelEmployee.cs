﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniSvOffice.Model
{
    public class ModelEmployee
    {
        public int EmploeeId { get; set; }
        public int RoomId { get; set; }
        public int UserId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
