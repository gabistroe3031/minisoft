﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniSvOffice.Model
{
    public class ModelOffice
    {
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }

        public virtual ICollection<ModelRoom> Rooms { get; set; }
    }
}
