﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MiniSvOfficeEntities.Data
{
    public partial class Room
    {
        public Room()
        {
            Employees = new HashSet<Employee>();
        }

        public int RoomId { get; set; }
        public int OfficeId { get; set; }
        public string RoomDesc { get; set; }

        public virtual Office Office { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
