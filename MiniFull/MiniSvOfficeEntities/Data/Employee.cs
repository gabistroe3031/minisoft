﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MiniSvOfficeEntities.Data
{
    public partial class Employee
    {
        public int EmploeeId { get; set; }
        public int RoomId { get; set; }
        public int UserId { get; set; }

        public virtual Room Room { get; set; }
    }
}
