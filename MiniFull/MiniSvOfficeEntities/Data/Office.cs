﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MiniSvOfficeEntities.Data
{
    public partial class Office
    {
        public Office()
        {
            Rooms = new HashSet<Room>();
        }

        public int OfficeId { get; set; }
        public string OfficeName { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
    }
}
