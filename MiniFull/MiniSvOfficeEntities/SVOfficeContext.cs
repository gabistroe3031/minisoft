﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using MiniSvOfficeEntities.Data;

#nullable disable

namespace MiniSvOfficeEntities
{
    public partial class SVOfficeContext : DbContext
    {
        public SVOfficeContext()
        {
        }

        public SVOfficeContext(DbContextOptions<SVOfficeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.EmploeeId);

                entity.HasIndex(e => e.UserId, "UK_OneUserOnly")
                    .IsUnique();

                entity.HasIndex(e => new { e.RoomId, e.UserId }, "UK_OneUserPerRoom")
                    .IsUnique();

                entity.Property(e => e.EmploeeId).HasColumnName("EmploeeID");

                entity.Property(e => e.RoomId).HasColumnName("RoomID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Employees_Rooms");
            });

            modelBuilder.Entity<Office>(entity =>
            {
                entity.Property(e => e.OfficeId).HasColumnName("OfficeID");

                entity.Property(e => e.OfficeName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.Property(e => e.RoomId).HasColumnName("RoomID");

                entity.Property(e => e.OfficeId).HasColumnName("OfficeID");

                entity.Property(e => e.RoomDesc)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Office)
                    .WithMany(p => p.Rooms)
                    .HasForeignKey(d => d.OfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Rooms_Offices");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
