﻿using AutoMapper;
using MiniSvUsers.Model;
using MiniSvUsersEntities.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniSvUsers.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ModelUser, User>();//.ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<User,ModelUser>();

        }
    }
}
