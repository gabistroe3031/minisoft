﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MiniSvUsers.Model;
using MiniSvUsersEntities;
using MiniSvUsersEntities.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniSvUsers.Controllers
{    

    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IMapper _mapper;
        protected readonly SVUsersContext _dbCtx;


        public UsersController(ILogger<UsersController> logger, IMapper mapper, SVUsersContext db)
        {
            _logger = logger;
            _mapper = mapper;
            _dbCtx = db;
        }

        [HttpGet]
        public IEnumerable<ModelUser> Get()
        {
            using (var ctx = _dbCtx)
            {
                var lstUsers = ctx.Users.ToList();
                IEnumerable<ModelUser> myList = _mapper.Map<IEnumerable<User>, IEnumerable<ModelUser>>(lstUsers);
                return myList;
            }           
        }
    }
}
