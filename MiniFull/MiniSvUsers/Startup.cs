using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MiniSvUsersEntities.Data;
using AutoMapper;
using MiniSvUsersEntities;
using RabbitMQManager;
using RabbitMQManager.DataStructure;
using MiniSvUsers.EventHandlers;
using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace MiniSvUsers
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SVUsersContext>(options =>
                                      {
                                          options.UseSqlServer(Configuration["ConnectionString"]);                                          
                                      });

            services.AddAutoMapper(typeof(Startup));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MiniSvUsers", Version = "v1" });
            });

            services.AddSingleton<IRabbitMq>(sp =>
            {
                var cfgRabbitMQHost = Configuration["RabbitMQHost"];
                var cfgRabbitMQUsername = Configuration["RabbitMQUsername"];
                var cfgRabbitMQPassword = Configuration["RabbitMQPassword"];
                var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
                var serviceBusConnection = new EventBusRabbitMQ(new RabbitMqConfiguration() { Hostname = cfgRabbitMQHost, Password = cfgRabbitMQPassword, UserName = cfgRabbitMQUsername, Queue = ConstantsMQ.QueueUsersOffice }, iLifetimeScope);

                return serviceBusConnection;
            });

            services.AddTransient<ReqUserDataIntegrationEventHandler>();            
        }



        public void ConfigureContainer(Autofac.ContainerBuilder builder)
        {
            //builder.RegisterModule<MyModule>();
        }

        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IRabbitMq>();

            eventBus.Subscribe<ReqUserData, ReqUserDataIntegrationEventHandler>();            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MiniSvUsers v1"));
            }
            
            ConfigureEventBus(app);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
