﻿using Microsoft.Extensions.Logging;
using MiniSvUsersEntities;
using RabbitMQManager.DataStructure;
using RabbitMQManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniSvUsers.EventHandlers
{


    public class ReqUserDataIntegrationEventHandler : IIntegrationEventHandler<ReqUserData>
    {
        private readonly SVUsersContext _repository;
        private readonly ILogger<ReqUserDataIntegrationEventHandler> _logger;

        public ReqUserDataIntegrationEventHandler(
            SVUsersContext repository,
            ILogger<ReqUserDataIntegrationEventHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Handle(ReqUserData @event)
        {
            await Task.CompletedTask;
        }
    }
}