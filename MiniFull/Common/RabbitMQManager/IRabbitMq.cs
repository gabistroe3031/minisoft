﻿using RabbitMQManager.DataStructure;
using RabbitMQManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQManager
{
    public interface IRabbitMq
    {

        void SendData(ADataIntegration theData);

        void Subscribe<T, TH>()
          where T : ADataIntegration
          where TH : IIntegrationEventHandler<T>;
    }
}
