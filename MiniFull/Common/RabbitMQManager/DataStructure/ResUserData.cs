﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQManager.DataStructure
{
    public class ResUserData : ADataIntegration
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public int UserID { get; set; }
    }
}
