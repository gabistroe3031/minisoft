﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQManager.DataStructure
{
    public class ReqUserData : ADataIntegration
    {

        public ReqUserData() : base()
        { 
        }

        public int UserID { get; set; }
    }
}
