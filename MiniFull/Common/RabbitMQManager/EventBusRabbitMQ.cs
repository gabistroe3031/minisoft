﻿using Autofac;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using RabbitMQManager.DataStructure;
using RabbitMQManager.Interfaces;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQManager
{
    public class EventBusRabbitMQ : IRabbitMq, IDisposable
    {

        const string BROKER_NAME = "_event_bus";

        private readonly string _hostname;
        private readonly string _username;
        private readonly string _password;
        private readonly string _queueName;
        private IModel _consumerChannel;
        private IConnection _connection;
        private ILifetimeScope _autofac;

        object sync_root = new object();

        public EventBusRabbitMQ(RabbitMqConfiguration rabbitMqOptions, ILifetimeScope autofac)
        {
            _hostname = rabbitMqOptions.Hostname;
            _username = rabbitMqOptions.UserName;
            _password = rabbitMqOptions.Password;
            _queueName = rabbitMqOptions.Queue;
            _autofac = autofac;
            _consumerChannel = CreateConsumerChannel();
        }

        public void Dispose()
        {
            _consumerChannel.Close();           
        }

        public void SendData(ADataIntegration theData)
        {
            var eventName = theData.GetType().FullName;

            using (var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: BROKER_NAME, type: "direct");

                var json = JsonConvert.SerializeObject(theData);
                var body = Encoding.UTF8.GetBytes(json);

                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = 2; // persistent                

                channel.BasicPublish(exchange: BROKER_NAME, routingKey: eventName, mandatory: true, basicProperties: properties, body: body);

            }
        }



        public void Subscribe<T, TH>()
          where T : ADataIntegration
          where TH : IIntegrationEventHandler<T>
        {
            string eventName = typeof(T).FullName;

            using (var channel = _connection.CreateModel())
            {
                channel.QueueBind(queue: _queueName,
                                  exchange: BROKER_NAME,
                                  routingKey: eventName);
            }

            StartBasicConsume(typeof(T), typeof(TH));
        }

        private void StartBasicConsume(Type requestType, Type hander)
        {

            var consumer = new EventingBasicConsumer(_consumerChannel);
            consumer.Received += (ch, ea) =>
            {
                using (var scope = _autofac.BeginLifetimeScope(BROKER_NAME))
                {
                    var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                    var integrationEvent = JsonConvert.DeserializeObject(content, requestType);
                    var handler_instance = scope.ResolveOptional(hander);
                    var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(requestType);
                    
                    _consumerChannel.BasicAck(ea.DeliveryTag, false);

                    concreteType.GetMethod("Handle").Invoke(handler_instance, new object[] { integrationEvent });                    
                }
            };

            _consumerChannel.BasicConsume(_queueName, false, consumer);
        }

        private IModel CreateConsumerChannel()
        {
            if (!IsConnected)
            {
                TryConnect();
            }

            _consumerChannel = _connection.CreateModel();

            _consumerChannel.ExchangeDeclare(exchange: BROKER_NAME, type: "direct");

            _consumerChannel.QueueDeclare(queue: _queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);

            /*
            _consumerChannel.CallbackException += (sender, ea) =>
            {
                if (_consumerChannel != null)
                {
                    _consumerChannel.Dispose();
                }
                _consumerChannel = CreateConsumerChannel();

                var consumer = new AsyncEventingBasicConsumer(_consumerChannel);

                consumer.Received += (ch, ea) =>
                {
                    var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                    return Task.CompletedTask;
                };
            };
            */
            return _consumerChannel;
        }

        private bool IsConnected
        {
            get
            {
                return _connection != null && _connection.IsOpen;
            }
        }

        private bool TryConnect()
        {            

            lock (sync_root)
            {
                var policy = RetryPolicy.Handle<SocketException>()
                    .Or<BrokerUnreachableException>()
                    .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                    {                        
                    }
                );

                policy.Execute(() =>
                {
                    var factory = new ConnectionFactory() { HostName = _hostname, UserName = _username, Password = _password };
                    _connection = factory.CreateConnection();
                });

                if (IsConnected)
                {                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}
